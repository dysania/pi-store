use rug::Float;

use std::collections::HashSet;
use std::time::Instant;

extern crate rayon;
use rayon::prelude::*;

extern crate num_cpus;

// use std::env;

fn main() {
    // let n: f64 = env::args().nth(1).unwrap().parse().unwrap();
    // search(n);

    // search_par();

    search_sqrt();
}

pub fn search(n: f64) {
    let now = Instant::now();
    let sqr = String::from(sqrt(n).split('.').nth(1).unwrap());
    let elapsed = (now.elapsed().as_millis() as f64) / 1000.0;
    eprintln!("sqrt({:5.1}) took {:6.2}s", n, elapsed);

    let now = Instant::now();
    let digits = set_all(4, sqr);
    let elapsed = (now.elapsed().as_millis() as f64) / 1000.0;
    eprintln!(" set({:5.1}) took {:6.2}s", n, elapsed);

    println!("{:5.1} {:9}", n, digits);
}

pub fn sqrt(n: f64) -> String {
    // magic number closest to the precision we need for n=434.5
    let precision = 985_004_644_u32;

    Float::with_val(precision, n)
        .sqrt()
        .to_string_radix(16, None)
}

pub fn set_all(chunk_size: usize, digits: String) -> u32 {
    let needed_chunks = (2 as usize).pow(((chunk_size as u32) / 2) * 8);
    let mut chunks = HashSet::new();

    let mut need_digits = 0u32;
    for i in 0..(digits.len() - chunk_size) {
        let chunk = String::from(&digits[i..(i + chunk_size)]);
        chunks.insert(chunk);

        if chunks.len() == needed_chunks {
            need_digits = (i + chunk_size) as u32;
            break;
        }
    }

    need_digits
}

pub fn search_par() {
    let num_threads = num_cpus::get();
    rayon::ThreadPoolBuilder::new()
        .num_threads(num_threads - 1)
        .build_global()
        .unwrap();
    let mut roots: Vec<f64> = Vec::new();

    for n in 2..4 {
        let n = n as f64;
        roots.push(n + 0.1);
    }

    roots
        .par_iter()
        .map(|&n| {
            let now = Instant::now();
            let sqr = String::from(sqrt(n).split('.').nth(1).unwrap());
            let elapsed = (now.elapsed().as_millis() as f64) / 1000.0;
            eprintln!("sqrt({:5.1}) took {:6.2}s", n, elapsed);
            (n, sqr)
        })
        .map(|n_sqr| {
            let (n, sqr) = n_sqr;
            let now = Instant::now();
            let digits = set_all(4, sqr);
            let elapsed = (now.elapsed().as_millis() as f64) / 1000.0;
            eprintln!(" set({:5.1}) took {:6.2}s", n, elapsed);
            (n, digits)
        })
        .for_each(|n_digits| {
            let (n, digits) = n_digits;
            println!("{:5.1} {:9}", n, digits);
        });
}

pub fn search_sqrt() {
    let num_threads = num_cpus::get();
    rayon::ThreadPoolBuilder::new()
        .num_threads(num_threads - 1)
        .build_global()
        .unwrap();

    let chunk_size = 4u32;
    let precision = 2u32.pow(chunk_size) + chunk_size;
    let max_set_size = 2usize.pow(chunk_size);

    // change the type for code below
    let chunk_size = chunk_size as usize;

    (2..2u32.pow(16)).into_par_iter().for_each(|n| {
        let n = n as f64;
        let sq = Float::with_val(precision, n)
            .sqrt()
            .to_string_radix(2, None);

        let digits = String::from(sq.split('.').nth(1).unwrap().split('e').nth(0).unwrap());

        // println!("{} {}", digits, n);

        let mut chunks = HashSet::new();

        for i in 0..=(digits.len() - chunk_size) {
            let chunk = &digits[i..(i + chunk_size)];
            let nib = u8::from_str_radix(chunk, 2).unwrap();

            // println!("{} {:0>8b}", chunk, nib);
            // assert!(chunk == format!("{:0>8b}", nib));

            chunks.insert(nib);

            if chunks.len() >= max_set_size {
                // println!("{:<10} {}", n, i + chunk_size);
                println!("{:<10} {}", n, digits);
                break;
            }
        }
    });
}

// pub fn parse_all(filename: &str) {
//     use std::str::FromStr;

//     struct Hex(u32);

//     impl FromStr for Hex {
//         type Err = String;

//         fn from_str(s: &str) -> Result<Self, Self::Err> {
//             if s.len() != 6 {
//                 return Err(format!("Incorrect length {}", s.len()));
//             }

//             let mut val = 0u32;

//             for n in s.chars() {
//                 let nibble = if n >= '0' && n <= '9' {
//                     (n as u8) - ('0' as u8)
//                 } else if n >= 'a' && n <= 'f' {
//                     (n as u8) - ('a' as u8) + 10u8
//                 } else if n >= 'A' && n <= 'F' {
//                     (n as u8) - ('A' as u8) + 10u8
//                 } else {
//                     return Err(format!("Not valid hex {}", n));
//                 };

//                 val = (val << 4) | (nibble as u32);
//             }

//             Ok(Hex(val))
//         }
//     }

//     let mut file = File::open(filename).unwrap();
//     let mut digits = String::new();
//     file.read_to_string(&mut digits).unwrap();
//     digits = String::from(digits.split_at(2).1);

//     let now = Instant::now();
//     for i in 0..(digits.len() - 6) {
//         let triplet = String::from(&digits[i..(i + 6)]);
//         let num: Hex = triplet.parse().unwrap();

//         assert_eq!(triplet, format!("{:0>6x}", num.0));

//         if i == 0 || i % 1_000_000 == 0 {
//             println!("parsed {}", i);
//         }
//     }

//     println!("All parsed correctly {}ms", now.elapsed().as_millis());
// }
